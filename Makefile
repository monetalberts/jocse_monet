paper: paper.tex
	pdflatex paper.tex
	pdflatex paper.tex
	bibtex paper
	pdflatex paper.tex
	pdflatex paper.tex
	open paper.pdf

clean: 
	rm paper.{aux,pdf,log,bbl,blg}
