\documentclass[jocse]{jocseart}
\usepackage{booktabs} % For formal tables
\newcommand{\ej}[1]{\textcolor{red}{\textit{#1} -- EJ}}

\setcopyright{jocsecopyright}
\jocseDOI{10.22369/issn.2153-4136/x/x/x }

% the following commands remove page numbers and headers that JOCSE formatting adds 
% in preparation for publication DO NOT REMOVE THESE
\pagestyle{plain} 
\pagenumbering{gobble}

\begin{document}
\title{STUDENT PAPER: Structure and dynamics of toughened epoxy thermosets using coarse-grained models and multithreaded simulations}

% Author(s) info
\author{Mone't Alberts}
\affiliation{%
  \institution{Micron School of Materials Science and Engineering\\Boise State University}
  \streetaddress{1910 University Dr.}
  \city{Boise}
  \state{ID}
  \postcode{83725}
}
\email{monetalberts@u.boisestate.edu}

\author{Eric Jankowski}
\orcid{0000-0002-3267-1410}
\affiliation{%
  \institution{Micron School of Materials Science and Engineering\\Boise State University}
  \streetaddress{1910 University Dr.}
  \city{Boise}
  \state{ID}
  \postcode{83725}
}
\email{ericjankowski@boisestate.edu}

\begin{abstract}
This paper provides a sample of a \LaTeX\ document for JOCSE submissions which conforms, somewhat loosely, to the formatting guidelines for
ACM SIG Proceedings.\footnote{This is an abstract footnote}
\end{abstract}

\keywords{HOOMD-Blue, epoxy, polymer, GPU, molecular dynamics, Blue Waters}
\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IN the intro we're goign to talk about the things that are missing
% we'll cite the papers that explain those things or, 
% that are examples of where people couldn't do what we're going to do
\section{Introduction}
Cross-linked polymers have widespread applications due to their chemical and mechanical properties\cite{Nielsen1969}, and are of particular importance in the aerospace industry\cite{Zhang2018d}.
In aerospace applications, cross-linked polymers are a component of polymer matrix composites (PMCs) that have advantages of high specific strength and specific modulus, particularly in the form of carbon fiber reinforced epoxy composites (CFRCs)\cite{Zhang2018d}.
These PMCs comprise 50\% of the weight of Boeing 787 and Airbus A350 airframes\cite{Marsh2008} and are widely deployed in ailerons, flaps, and landing-gear doors. 

The matrix of PMCs is termed \textit{thermoset} because the liquid precursors (e.g.~epoxy and amine monomers) mechanically \textit{set} as the temperature is increased to initiate the cross-linking reactions.
Epoxy-amine thermosets are typically brittle, and are made tougher by introducing polymers with low glass transition temperatures $T_g$\cite{Liu2016h}.
Un-reacted mixtures of epoxy, amine, and thermoset precursors are termed \textit{resins} and are used to impregnate tapes of fiber to be used in composite materials \cite{Ratna2009,Lipic1998}.
Laminates are made by stacking or weaving tapes of impregnated fibers and used as the building-blocks of aerospace parts such as ailerons and flaps \cite{Fredi2018}.
The curing of parts can vary, occurring at elevated or room temperatures, in vacuum-bags or in autoclaves as examples, but in all cases the reactive amine and epoxide monomers cross-link and form a matrix whose microstructure influences the properties of the cured part.%TODO: Cite?
During curing the toughening agent may remain well-mixed within the matrix, or may separate from the thermoset to varying degrees.
Also during curing, stresses may build, causing warping of the parts.
The overall challenge to manufacturing aerospace parts from thermosets is choosing the precursors and processing protocols that enable parts to be manufactured as quickly as needed, that do not warp, that have microstructured matrices giving mechanical properties within specifications.

In this work we focus on understanding epoxy thermosets in microscopic detail through the lens of molecular simulations. 
Extensive prior work using molecular dynamics and Monte Carlo simulations of thermosets is reviewed by Li and Strachan in their 2015 work \cite{Li2015f}, summarizing the techniques used to predict polymer network structure, gelation, $T_g$, coefficient of thermal expansion, heat capacity, and Young's modulus. 
Molecular simulations provide crucial insight into the engineering of thermosets because they enable the connection of atomic details to macroscopic properties in ways that are impractical or intractable to experiments: The bonding networks of thermosets are difficult to resolve with scattering experiments, the percentage of cure ($\alpha$) must typically be inferred indirectly via rheology or spectroscopy, and nanostructured phase separation may be difficult to detect with microscopy techniques, demanding small-angle scattering techniques. %TODO: Cite?
The challenge to using molecular simulations to predict material properties of thermosets is the classic length-time tradeoff of materials simulations: Accessing volumes needed to resolve microphase separation may require millions of simulation elements $N$ \cite{epoxpy}, but the time it takes to advance towards equilibrium scales worse-than-linearly with $N$. 
Two techniques can help with the length-time tradeoff: (1) using high performance computers such as graphics processing units to parallelize independent computations\cite{epoxpy}, and (2) using simplified models to reduce the $N$ needed to represent the system of interest\cite{Jankowski2019}.
Here we apply both techniques, using a coarse-grained model of toughened epoxy thermosets that is implemented on GPUs using HOOMD-Blue\cite{epoxpy,Anderson2008,Anderson2020} to predict structure and dynamics of model epoxy thermosets.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Model}
Epoxy, amine, and toughener monomers are represented by spherical simulation elements (Figures 1 and 2), a coarse-graining scheme that enables simulations to be performed with over 20 times smaller $N$ than an all-atom representation.
In doing so we were able to represent a large simulatoin volume with realistic mixtures of components while simultaneously decreasing the time it takes to advance between simulation configurations.
The amine monomers are represented by red beads labeled `A', the epoxy monomers are represented by blue beads labeled `B' and the tougheners are represented by green beads labeled `C' (Figure~\ref{cgb}).
Iteraction parameters between amine (A), epoxy (B), and toughener (C) simulation elements are identical: $\epsilon_{AA} = \epsilon_{BB} = \epsilon_{CC} = 1\epsilon$ and $\sigma_A=\sigma_B=\sigma_C=1\sigma$, so all pairwise interactions are described by the Lennard-Jones interaction potential
\begin{equation}
U_{LJ}(r) = 4\epsilon\left(\left(\frac{\sigma}{r}\right)^{12}-\left(\frac{\sigma}{r}\right)^{6}\right),
\label{lj}
\end{equation}
where $r$ is the center-to-center distance between a pair of particles.
Equation~\ref{lj} has a global minimum at $U_{LJ}(r=2^{1/6}\sigma)=\epsilon$, so $\epsilon$ is interpreted as a measure of the qualitative attractions among model elements A, B, and C. 

The chemical bonding of A and B species is modeled stochastically, as described in detail in Refs~\cite{epoxpy,epoxpy-zenodo}.
Briefly, an amine and epoxy may bond with probability $\exp(-E_a/k_BT)$ if they are within a distance of $1.2\sigma$ during a bonding step.
We model the A amines as tetrafuctional- they can bond up to 4 unique B epoxy beads (Figure 1).
The epoxy beads (B) can bond up to 2 unique A beads (Figure~\ref{bonds}).
We consider unreacted amines as ``primary'' amines with $E_{a,1}=4\epsilon$ and consider any other amine to be a ``secondary'' amine with
\begin{equation}
E_{a,2} = B * E_{a,1},
\end{equation}
where $B$ is the \textit{bond factor} that we vary to test the effect of different secondary bond activation energies.
Here, $B \in \left[ 1, 0.5, 0.25, 0.1, 0.033\right] $. 
As $B$ is decreased, it is less likely that a bead of A or B will bond again once it has already made an initial bond. 
After bonds are formed between amine and epoxy simulation elements, the equilibrium bond distance of each is set to $r_0=1\sigma$, with a spring constant of $k=100 \frac{\epsilon}{\sigma^2}$.
For both toughener simulation elements (C) and crosslinked thermoset simulation elements (A,B), no angle constraints are implemented between triplets of bonded particles, nor dihedrals between quadruplets.
Tougheners molecules are represented by chains of 10 C beads and do not participate in bonding. 

\begin{figure}
\centering
\includegraphics[width=0.95\columnwidth]{figs/Beads.pdf}
    \caption{Coarse grained representation of epoxy and amines. Dashed lines reprepresent possible bonds that can be formed with the other species: Amine beads can bond up to four epoxies and epoxies can bond up to two amines.}
\label{cgb}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.95\columnwidth]{figs/Toughener.pdf}
    \caption{Coarse grained represenation of toughener molecules. Chains of 10 simulation elements are joined by harmonic springs without angle or dihedral constraints.}
\label{tough}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.95\columnwidth]{figs/bonding2.pdf}
    \caption{Schematic bonding formation between one amine and three epoxy monomoers. Bonds are modeled with harmonic springs linking the centers of simulation elements with equilibrium separation $r_0=1\sigma$ and spring constant $k=100\epsilon/\sigma^2$.}
\label{bonds}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In the methods section we will talk about the simulations that were run and why we decided that they would be important
\section{Methods}
Reacting molecular dynamics simulations are performed using HOOMD-Blue on NVIDIA GPUs, with radial distribution functions ($g(r)$) and mean-squared displacements calculated to analyze the structure and dynamics of the simulation trajectories, respectively. 
Simulations are initialized with independent random configurations of 10,000 A particles, 20,000 B particles, and 2,000 linear chains of 10 C particles in cubic periodic simulation volumes of side length $36.84\sigma$ resulting in number density $\eta=1\frac{\textrm{particle}}{\sigma^3}$.
Simulations are performed in the canonical (constant number of particles $N$, volume $V$, and temperature $T$) ensemble using the Nos\'{e}-Hoover thermostat \cite{Hoover1985} and the MTK equations \cite{Martyna1994,Cao1996} with the caveat that dynamic bond formation continually updates the simulation degrees of freedom and one should not consider the microstates sampled to be representative of equilibrium.
A step size of $0.01\tau$ is used to advance simuations for $5\times 10^6$ time steps, where the time unit $\tau$ can be mapped to a specific physical system with $\tau=\sqrt{\frac{M\sigma^2}{\epsilon}}$ for dimensional units of energy $\epsilon$, distance $\sigma$, and mass $M$.

Reduced temperature units $T=\frac{k_BT_K}{\epsilon}$ are used throughout this work, and provide a mapping between temperature in Kelvin $T_K$ and $T$ with Boltzmann's constant $k_B$ and energy unit $\epsilon$.
Reduced units enable the same simulations to be mapped to multiple experimental systems (by substituting in appropriate $\sigma$, $\epsilon$, and $M$ units).
Reduced units are also convenient in that measurements around 1 are a useful reference point: Around $T=1$ thermal fluctuations from particle motions are of the same order of magnitude as the Lennard-Jones well depth $\epsilon$; Around $r=1\sigma$ spherical particles are tangent to each other; Around $g(r)=1$ the liklihood of finding two particles separated by $r$ is the same as uniformly distributed particles at the same density. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simulations and Results}
Production simulations are performed for combinations of $B$ and $T$ within the parameter space $B\in \left[1, 2, 4, 10, 30 \right]$ and $T\in \left[ 1, 10, 30, 50\right]$.
Here we describe structural observations as measured by pair correlation functions  $g(r)$ and dynamical observations as measured by mean-squared displacements (MSDs).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Structure}
We first focus on the nanoscale structure of the cured toughened thermosets. 
Cure fractions $\alpha$ after $5\times 10^6$ steps are summarized in Table~\ref{table1}.
We find that at constant $T$, the highest $\alpha$ occur for lowest $B$, as expected: The lower the secondary bond activation energy (lower $B$), the more likely the formation of secondary bonds, all other things equal.
At constant $B$ we observe small increases in $\alpha$ between $T=1$ and $T=10$, and that $\alpha$ remains relatively constant for $T\ge10$.
These findings are consistent with our expectations, save the saturation in $\alpha$ at high $T$ when $B$ is held constant. 
We interpret the saturation of $\alpha$ at high $T$ to indicate that structural factors prevent A and B simulation elements from coming in close proximity (such as exclusion of unbonded simulation from a densely crosslinked mass with few surface bonds available), and note the difference in $\alpha$ as T increases from $T=10$ to $T=50$ is smaller than 0.9 in each case, and so are an anomoly with relatively small magnitude.
In short, the morphologies studied here represent a range cure percentages from 75.5\% to 97.2\%, which are appropriate for comparison of cured toughened thermosets from experiments. \ej{Need to cite some experiments here or in intro.}

\begin{table}[h]
    \caption{Summary of simulation state space (temperature $T$ and bond factor $B$), and the resulting performance in time-steps-per-second (TPS) and cure fraction $\alpha$. }
\centering
\begin{tabular}{|c|c|c|c|}
\hline
    $T$ & $B$ & TPS & $\alpha$      \\
    \hline  1 &   1   & 73.6 & 93.2 \\    %Time: 18:52:52
    \hline 10 &   1   & 70.6 & 97.2 \\    %Time: 19:40:29
    \hline 50 &   1   & 37.4 & 96.1 \\    %Time: 37:09:27
    \hline  1 &   2   & 76.1 & 90.0 \\    %Time: 18:15:09
    \hline 10 &   2   & 77.7 & 96.5 \\    %Time: 17:53:06
    \hline 50 &   2   & 47.4 & 95.8 \\    %Time: 29:19:34
    \hline  1 &   4   & 94.7 & 84.9 \\    %Time: 14:39:48
    \hline 10 &   4   & 61.3 & 94.7 \\    %Time: 22:38:53
    \hline 30 &   4   & 62.6 & 94.8 \\    %Time: 22:11:52
    \hline 50 &   4   & 38.8 & 94.8 \\    %Time: 35:45:21
    \hline 10 &  10   & 79.6 & 88.7 \\    %Time: 17:27:12
    \hline 30 &  10   & 57.3 & 89.5 \\    %Time: 24:15:22
    \hline 50 &  10   & 49.2 & 89.5 \\    %Time: 18:14:16
    \hline 10 &  30   & 71.2 & 75.6 \\    %Time: 19:30:01
    \hline 30 &  30   & 65.8 & 75.5 \\    %Time: 21:06:32
    \hline 50 &  30   & 59.8 & 75.7 \\    %Time: 23:13:10
\hline
\end{tabular}
\label{table1}
\end{table}

We next consider the spatial distribution of the A, B, and C species to analyze structure.
In the case of $B=1$, which has the highest likelihood of secondary bond formation across all $B$, we observe phase separation across all temperatures (Figure~\ref{BW1}).
The degree of phase separation varies with temperature, and best represented by $g_C(r)$ (lower left of Figure~\ref{BW1}). 
The \ej{update captions, describe structure.}
Phase separation of the toughener molecules (green, C) from the crosslinked A-B simulation elements is visualized in simulation snapshots, and is measured by long-range deviation of $g(r)$ from ideal behavior. 
That is, a large, broad peak ($g(r>2)>1$) is representative of simulation elements clustering and not being uniformly distributed in the simulation volume, and can be seen in the $g_{\textrm{all}}(r)$, $g_{A,B}(r)$, and $g_C(r)$ for T=1. 
The degree to which the crosslinked A-B and the tougheners separate and fill the volume differ as a function of temperature.
As $T$ is increased we observe a more dense volume of crosslinked A-B simulation elements, enabling the toughener (green, C) chains to fill a relatively larger volume.
The spatial spreading of the phase-separated tougheners is measured by the deacrease in $g_C(r)$ as T is increased (Figure~\ref{BW1}, bottom row), which approaches ideal ($g(r)=1$) behavior at the highest temperatures.

\begin{figure}
\centering
\includegraphics[width=3.5in]{figs/BW1_Structure.png}
    \caption{$B=1$ structure summary. Each row includes $g(r)$ and representative simulation snapshots at three different temperatures. The first row includes all amine, epoxy, and toughener beads; the second row includes only amine and epoxy beads, and the third row includes only toughener beads. }
\label{overlap}
\end{figure}

In the case of $B=2$ (Figure~\ref{BW2}), structures are qualitatively and quantitatively identical to those of $B=1$.
That these two different secondary bond factor cases would result in identical morphologies is not obvious, because the $B=2$ cases have lower $\alpha$ than the $B=1$ cases. 
TODO, a bit more on structures we see here.
\begin{figure}
\centering
\includegraphics[width=3.5in]{figs/BW2_Structure.png}
    \caption{$B=2$ structure summary. Each row includes $g(r)$ and representative simulation snapshots at three different temperatures. The first row includes all amine, epoxy, and toughener beads; the second row includes only amine and epoxy beads; and the third row includes only toughener beads. }
\label{BW2}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=3.3in]{figs/overlap.png}
    \caption{Detail of a simulation snapshot with ``overlapping'' simulation beads. Grid lines represent a spacing of $1\sigma$. The bonds rendered bonds are approximately the expected length ($1\sigma$), however many particles (two blue spheres in upper left, two red spheres in lower right) sit nearly on top of each other. These observations confirm the spatial correlations measured in g(r) and indicate a model shortcoming: The interactions among epoxy and amine species, specifically the volumes they fill, should prevent such overlaps from occurring. This obervation hints that some pairs of interactions are being erroneously omitted, and updates to the pairwise exclusion lists used during force calculations are warranted.}
\label{overlap}
\end{figure}

The first thing that we might notice from the secondary bondweight of 10 simulations is the y-axis of their rdf's. The rdf's have lower peaks at a bondweight 
of 10 than they do at a bondweight of 1 or 2, which is to be expected. At a lower bondweight we are more likely to see a closer neighbor bead than we are at 
a higher bondweight, so the peaks at 1 and 2 should be higher. As temperature increases the trend of the C's spreading through the simulation box is the same,
however, we never see big clumps of C because the A's and B's are now able to fill the simulation box before that happens. 
At a bondweight of 10 we almost see the opposite in how the A's and B's act as temperature is increases because the bondweight is higher, they are less likely to make a 
secondary bond, so at low temperatures the A's and B's are more spread out. But as we increase temperature, A's and B's are more likely to make a secondary bond and therefore they start to clump more at high temperatures. 
\begin{figure}
\centering
\includegraphics[width=3.5in]{figs/BW10_Structure.png}
    \caption{$B=10$ structure summary. Each row includes $g(r)$ and representative simulation snapshots at three different temperatures. The first row includes all amine, epoxy, and toughener beads; the second row includes only amine and epoxy beads; and the third row includes only toughener beads. }
\label{BW10}
\end{figure}

The last bondweight to compare is a bondweight of 30 (Figure~\ref{BW30}).
\begin{figure}
\centering
\includegraphics[width=3.5in]{figs/BW30_Structure.png}
    \caption{$B=30$ structure summary. Each row includes $g(r)$ and representative simulation snapshots at three different temperatures. The first row includes all amine, epoxy, and toughener beads; the second row includes only amine and epoxy beads; and the third row includes only toughener beads. }
\label{BW30}
\end{figure}

If we take a look at the y-axis of the rdf's again we will see that the peaks are lower, however, this time they are significantly lower, which is reflected in the morphologies.
Each of the morphologies looks nearly identical and mostly mixed. There are no clumps of beads. If we compare this to a bond weight of 1 we see that there is a significant
difference. Digging deeper into the rdf's we can see that at T=10, all of the rdf's settle down to 1, meaning that at a lower temperature the system is mixed almost completely
with no clumps. If the temperature is increased, we see that the rdf's of the A's and B's no longer settle to 1, while the rdf's of the C's still do. The C's are still filling the simulation space but the A's and B's are beginning to clump slightly, however, these clumps can not be seen in the morphologies and are not nearly as significant as 
a bondweight of 1, 2, or 10. 

From all of these we can see the effect that increasing the secondary bond weight has on morphology and use any trends we find to run simulations with a secondary bond weight 
that would produce a morphology that's comparable to experiment.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dynamics}
As temperature increases, diffusivity increases monotonically and as secondary bond probability decreases, diffusivity increases. 
The diffusivity of the C beads is always higher than that of the A and B beads. 
This can be attributed to the fact that the C beads do not make any bonds and are free to move around the simulation space, whereas the A and B beads will create bonds, which 
changes how freely they are able to move throughout the space, depending on how many bonds they make.

\begin{figure}
\centering
\includegraphics[width=3.5in]{figs/MSD_ALL.png}
    \caption{Diffusivity of A, B, and C beads versus temperature with secondary bond factors of 0.5, 0.25, 0.1, and 0.033.}
\label{Diff1}
\end{figure}

Overall, the diffusivity calculations of each system correlate well to the cure percentage of that system. 
At a secondary bondweight probability of 1/30, the cure percent of each system was approximately 75 despite any changes in temperature. 
The diffusivity of the A and B beads at a secondary bond probability of 1/30 is at its highest, indicating that these beads are moving more freely within the space than they 
are when they have a higher chance of creating a secondary bond. 
The diffusivity of the C beads at this same bond probability is at its lowest, indicating that the movement of the A and B beads is inhibiting the C beads from moving as freely as they did then the A and B beads were making more bonds. 
If we compare the rdf's and morphologies of a secondary bondweight of 1 and a secondary bondweight of 2 there are no significant differences.
Not only do they have similar cure percents, their rdf's are nearly identical, as are their diffusivities. 
All of the rdf's are nearly the same, and all of the morphologies have the same amount of clumped A-B's and C's. The difference comes when
different temperatures are compared which is expected. As temperature is increased, the C beads are able to fill the space of the simulation box
quicker than they can at low temperatures and we see this in the morphologies. In both figures one and two, at T=50, the C's are able to nearly
fill the entire simulation box versus clumping or leaving holes, and the AB beads fill up the rest of the space that is available. The rdf's of the C beads at
at T=50 settles to 1, which indicates that there aren't any clumps of C beads and they are mostly spread out, which we also see corresponds with the morphology. 
Higher bond weights show more interesting differences than bondweights 1 and 2, so lets take a look at those next.


WHAT WE KNOW NOW:
From these jobs we discovered a few things that we didn't know before. One being what effect the secondary bond weight has on the morphology within the simulation box. By varying the temperature of the simulation we also were able to determine how long a simulation needed to be run with a specific temperature and secondary bond weight to reach equilibrium. This gave us some insight on the parameters that we needed to use and how long it would take the simulations to run to get equilibriated results. \ej{todo}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
%Here's where we wrap everything up.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Student Reflection}
At the two-week Petascale Institute at the University of Illinois, Urbana-Champaign I gained knowledge of HPC infrastructure, learned how to code in C, and I gained meaningful connections with the scientists from Shodor Education Foundation, NCSA, and other students from across the US.
I also simultaneously was able to showcase knowledge I didn't know I had built while gaining deeper HPC expertise while debugging my own project.
For example, I set up shell environenments and conda environments, I loaded modules to suppoort my software stack, I compiled our MD engine HOOMD from source, I worked around the permissions limitations of directories software expected to write to, and was able to find ways to run my simulations.
This kind of troubleshooting was a majority of my petascale institute and internship experience.
It took us almost 7 months to get HOOMD Blue to run on Blue Waters which is essential to the simulations that we run.
I constantly referred to the institute lesson plans while working to get my simulations running and once I was finally successful I used my entire Blue Waters allocation in two weeks.

Another challenge that I faced was understanding the results of the work that we were doing.
When I began this project I was coming out of my freshman year in Materials Science.
I understood the concept of putting numbers into a simulation and pressing "Go" and getting figures and data at the end, however, I didn't fully understand what happened after I pressed "Go", I didn't understand what each line of code meant and how it related to real world science.
Through the Blue Waters internship I was able to more deeply understand the what made the simulations run, which helped provide space for understanding the real world science behind them. 

The Blue Waters Petascale Institute was foundational in my development and confidence as a computational scientist.
Now, I am able to answer questions I didn't even know that I knew the answer to.
Before when I would get an error code I would immediately ask someone else to help or I would have a little bit of anxiety about how to solve the problem, now I know how to troubleshoot error codes.
I know where to look to get the answers that I need and I know what questions to ask if I need help.
Eventually I would have gotten to this point in coding and my research, but I believe that being a Blue Waters intern propelled that learning because there were a lot of problems that I needed to figure out beyond the research that I was doing.
I didn't realize that I had any of this growth until last summer when I was tasked with helping new lab members learn the ins and outs of research and coding, and I helped a new Blue Waters intern get her simulations initialized using blue waters.
Although I didn't have anywhere close to all of the answers to the questions that I was asked, I had answers that I didn't know I had.
I had knowledge that I hadn't even realized that I learned.

Through the Blue Waters internship and my work in the Computational Materials Engineering Lab at Boise State University, I have found a passion for high performance computing.
The technical skills that I gained have proven to be invaluable to my education and career, and I expect them to continue to do so I learned that beyond the scope of my own work, HPC has a far reach within the scientific community, and can be used for the benefit of all research, no matter the topic.
I plan to use all that I have learned to aid in my next endeavor in the pursiut of a Ph.D. in Biomedical Engineering at Boise State University, which I will begin in the Fall of 2020.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Acknowledgments}
This research is part of the Blue Waters sustained-petascale computing project, which is supported by the National Science Foundation (awards OCI-0725070 and ACI-1238993) and the state of Illinois. Blue Waters is a joint effort of the University of Illinois at Urbana-Champaign and its National Center for Supercomputing Applications.
We would like to acknowledge high-performance computing support of the R2 compute cluster (DOI: 10.18122/B2S41H) provided by Boise State University's Research Computing Department.
This work used the computing resources supported by Boise State College of Engineering Information Technology services.
This material is based upon work supported by The Boeing Company under contract BRT-LO217-0072.

\bibliographystyle{unsrt}
\bibliography{library}  % 
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

